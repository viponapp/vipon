import { extendObservable, action } from 'mobx'

class Deal {
  constructor ({ id, name }) {
    this.id = id

    extendObservable(this, {
      name,
    })
  }
}

class DealStore {
  constructor () {
    extendObservable(this, {
      deals: [],
      loading: false,
      loadRegions: action(() => {
        this.loading = true
        // do something
        this.loading = false
      }),
    })
  }
}

const dealStore = new DealStore()
export default dealStore
