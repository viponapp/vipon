import 'normalize.css'
import 'airbnb-js-shims'
import initReactFastclick from 'react-fastclick'
import React from 'react'
import ReactDOM from 'react-dom'
import { Router, Route, IndexRoute, browserHistory, applyRouterMiddleware } from 'react-router'
import { useScroll } from 'react-router-scroll'
import App from './components/App'
import Index from './pages/Index'
import NotFound from './pages/NotFound'
import Deal from './pages/Deal'
import Join from './pages/Join'
import Login from './pages/Login'
import Requests from './pages/Requests'
import Account from './pages/Account'

initReactFastclick()

ReactDOM.render(
  <Router history={browserHistory} render={applyRouterMiddleware(useScroll())}>
    <Route path='/' component={App}>
      <IndexRoute component={Index} />
      <Route path='join' component={Join} />
      <Route path='login' component={Login} />
      <Route path='requests' component={Requests} />
      <Route path='account' component={Account} />
      <Route path=':dealSlug' component={Deal} />
      <Route path='*' component={NotFound} />
    </Route>
  </Router>,
  document.getElementById('root'),
)
