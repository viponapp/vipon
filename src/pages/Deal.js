import React from 'react'
import styled from 'styled-components'
import { compose, withProps } from 'recompose'
import Detail from '../components/deal/DealDetail'
import deals from '../data/deals'
import media from '../utils/media'
import Icon24 from '../components/common/Icon24'
import ButtonTransparent from '../components/common/ButtonTransparent'
import { gray } from '../components/common/colors'

const Wrap = styled.div`
  ${media.medium`padding: 24px 0;`}
`
const Container = styled.div`
  max-width: 1360px;
  margin: 0 auto;
  ${media.medium`padding: 0 48px;`}
`
const RelativeWrap = styled.div`
  position: relative;
`
const ButtonWrap = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  padding: 8px;
`

const Deal = ({ deal, overlay }) => (
  <Wrap>
    <Container>
      <RelativeWrap>
        {overlay && (
          <ButtonWrap>
            <ButtonTransparent>
              <Icon24 close fill={gray} />
            </ButtonTransparent>
          </ButtonWrap>
        )}
        <div onClick={e => e.stopPropagation()}>
          <Detail deal={deal} />
        </div>
      </RelativeWrap>
    </Container>
  </Wrap>
)

export default compose(
  withProps(({ params }) => {
    let dealId = params.dealSlug.split('-')
    dealId = dealId[dealId.length - 1]

    return {
      deal: deals.find(deal => deal.id === dealId),
    }
  }),
)(Deal)
