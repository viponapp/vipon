import React from 'react'
import styled from 'styled-components'
import { compose, withProps } from 'recompose'
import AccountNav from '../components/AccountNav'
import Input from '../components/common/Input'
import Container from '../components/common/Container'
import Button from '../components/common/Button'
import Checkbox from '../components/common/Checkbox'

const Form = styled.div`
  max-width: 512px;
  min-height: 480px;
  padding: 64px 0;
`
const InputWrap = styled.div`
  padding: 32px 0 0;
  &:first-child { padding: 0; }
`
const CheckboxWrap = styled.div`
  padding: 24px 0 0;
`
const ButtonContainer = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 32px 0 0;
`

const Account = ({ email, password }) => (
  <div>
    <AccountNav />
    <Container>
      <Form>
        <InputWrap>
          <Input underline value={email} type='email' />
        </InputWrap>
        <InputWrap>
          <Input underline value={password} type='password' />
        </InputWrap>
        <CheckboxWrap>
          <Checkbox checked label='Send me a daily summary of approved deal requests' />
        </CheckboxWrap>
        <ButtonContainer>
          <Button blue>Update details</Button>
          <Button gray>Sign out</Button>
        </ButtonContainer>
      </Form>
    </Container>
  </div>
)

export default compose(
  withProps({
    email: 'sara.smith@gmail.com',
    password: '123456789',
  }),
)(Account)
