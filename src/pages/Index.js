import React from 'react'
import { compose, withProps } from 'recompose'
import styled from 'styled-components'
import dealsData from '../data/deals'
import Hero from '../components/Hero'
import Container from '../components/common/Container'
import DealList from '../components/deal/DealList'
import Filters from '../components/Filters'

const Wrap = styled.div`
  padding: 16px 0 32px;
`

const Index = ({ deals }) => (
  <div>
    <Hero />
    <Container>
      <Filters />
      <Wrap>
        <DealList deals={deals} />
      </Wrap>
    </Container>
  </div>
)

export default compose(
  withProps(() => ({
    deals: dealsData,
  })),
)(Index)
