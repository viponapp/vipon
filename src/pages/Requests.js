import React from 'react'
import styled from 'styled-components'
import { compose, withProps } from 'recompose'
import Container from '../components/common/Container'
import AccountNav from '../components/AccountNav'
import RequestList from '../components/request/RequestList'
import Text from '../components/common/Text'

const Page = styled.div`
  padding: 0 0 64px;
`
const Header = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  padding: 64px 0 0;
`
const Separator = styled.div`
  width: 1px;
  height: 24px;
  margin: 0 16px;
  background: #e8e8e8;
`
const CountWrap = styled.div`
  background: #d8d8d8;
  padding: 8px 12px;
  margin: 0 8px 0 0;
  border-radius: 100px;
`
const ListWrap = styled.div`
  padding: 48px 0 0;
`

const Requests = ({ requests, requestsLeft }) => (
  <Page>
    <AccountNav />
    <Container>
      <Header>
        <Text large noWrap>Deal Requests</Text>
        <Separator />
        <CountWrap>
          <Text small noWrap>{requestsLeft}</Text>
        </CountWrap>
        <Text small noWrap>Vouchers Remaining</Text>
      </Header>
      <ListWrap>
        <RequestList requests={requests} />
      </ListWrap>
    </Container>
  </Page>
)

export default compose(
  withProps({
    requests: [
      {
        deal: {
          coverPhoto: require('../static/plant.png'),
          name: 'COMI Car Charger CG081 Air Purifier Bluetooth 4.0 Earphone Dual USB Headset Android IOS Smart Phone Oxygen Bar Cleaner (GOLD)',
          price: 24.99,
          finalPrice: 8.99,
          supportEmail: '2850732890@qq.com',
        },
        status: 'approved',
        accepted: true,
        // turn into @computed prop
        requested: 'Yesterday',
        coupon: 'YVZU-FTB2HV-NR6HFC',
      },
      {
        deal: {
          coverPhoto: require('../static/plant.png'),
          name: 'COMI Car Charger CG081 Air Purifier Bluetooth 4.0 Earphone Dual USB Headset Android IOS Smart Phone Oxygen Bar Cleaner (GOLD)',
          price: 24.99,
          finalPrice: 8.99,
          supportEmail: '2850732890@qq.com',
        },
        status: 'pending',
        // turn into @computed prop
        requested: 'Yesterday',
      },
    ],
    requestsLeft: 144,
  }),
)(Requests)
