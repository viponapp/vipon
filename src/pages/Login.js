import React from 'react'
import Lock from '../components/Lock'

const Login = () => (
  <Lock login />
)

export default Login
