import moment from 'moment'
import shortid from 'shortid'

// NOTE: This is just dummy data. This should be living in a proper database,
// and the data should be fetched over a proper API and stored in a client-side state/cache (see Redux or Mobx)
//
// Also when coding the deals store in Mobx, make sure there's a computed `expired`
// and a computed `timeLeft` for every deal. The time left of a deal is
// currently not being updated reactively across the UI.

shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@')

export default [
  {
    id: shortid.generate(),
    name: 'Cube Small Planter, Clear',
    price: 8.99,
    priceBefore: 24.99,
    category: 'Home & Kitchen', // ?
    expireAt: moment().add(45, 'm').toISOString(),
    coverPhoto: require('../static/basket.png'),
    photos: [
      require('../static/basket.png'),
      require('../static/plant.png'),
    ],
    description: 'The Ceramic Bowls with Metal Tri-Stand Planter was originally designed during the post-WW2 architecture and design period. The ceramic planter is defined by its basic geometric shapesthree half-spheres and a metal triangleand its simple, clean finish. The Bowls are designed to be used in both indoor and outdoor spaces, and is supported by a powder-coated steel stand. Although this planter was originally conceived over sixty years ago, it retains a contemporary, current look. In addition to its function as a tri-planter, each of the bowls can be removed and placed on the ground, a tabletop, or other surface to create a new look.',
  },
  {
    id: shortid.generate(),
    name: 'Cube Small Planter, Clear',
    price: 8.99,
    priceBefore: 24.99,
    category: 'Home & Kitchen',
    expireAt: moment().add(45, 'm').toISOString(),
    coverPhoto: require('../static/plant.png'),
    photos: [
      require('../static/plant.png'),
      require('../static/basket.png'),
      require('../static/plant.png'),
    ],
    description: 'The Ceramic Bowls with Metal Tri-Stand Planter was originally designed during the post-WW2 architecture and design period. The ceramic planter is defined by its basic geometric shapesthree half-spheres and a metal triangleand its simple, clean finish. The Bowls are designed to be used in both indoor and outdoor spaces, and is supported by a powder-coated steel stand. Although this planter was originally conceived over sixty years ago, it retains a contemporary, current look. In addition to its function as a tri-planter, each of the bowls can be removed and placed on the ground, a tabletop, or other surface to create a new look.',
  },
  {
    id: shortid.generate(),
    name: 'Cube Small Planter, Clear',
    price: 8.99,
    priceBefore: 24.99,
    category: 'Home & Kitchen',
    expireAt: moment().add(45, 'm').toISOString(),
    coverPhoto: require('../static/forgot.png'),
    photos: [
      require('../static/forgot.png'),
      require('../static/basket.png'),
      require('../static/plant.png'),
    ],
    description: 'The Ceramic Bowls with Metal Tri-Stand Planter was originally designed during the post-WW2 architecture and design period. The ceramic planter is defined by its basic geometric shapesthree half-spheres and a metal triangleand its simple, clean finish. The Bowls are designed to be used in both indoor and outdoor spaces, and is supported by a powder-coated steel stand. Although this planter was originally conceived over sixty years ago, it retains a contemporary, current look. In addition to its function as a tri-planter, each of the bowls can be removed and placed on the ground, a tabletop, or other surface to create a new look.',
  },
  {
    id: shortid.generate(),
    name: 'Cube Small Planter, Clear',
    price: 8.99,
    priceBefore: 24.99,
    category: 'Home & Kitchen',
    expireAt: moment().add(45, 'm').toISOString(),
    coverPhoto: require('../static/alarm.png'),
    photos: [
      require('../static/alarm.png'),
      require('../static/basket.png'),
      require('../static/plant.png'),
    ],
    description: 'The Ceramic Bowls with Metal Tri-Stand Planter was originally designed during the post-WW2 architecture and design period. The ceramic planter is defined by its basic geometric shapesthree half-spheres and a metal triangleand its simple, clean finish. The Bowls are designed to be used in both indoor and outdoor spaces, and is supported by a powder-coated steel stand. Although this planter was originally conceived over sixty years ago, it retains a contemporary, current look. In addition to its function as a tri-planter, each of the bowls can be removed and placed on the ground, a tabletop, or other surface to create a new look.',
  },
  {
    id: shortid.generate(),
    name: 'Cube Small Planter, Clear',
    price: 8.99,
    priceBefore: 24.99,
    category: 'Home & Kitchen',
    expireAt: moment().add(45, 'm').toISOString(),
    coverPhoto: require('../static/alarm.png'),
    photos: [
      require('../static/alarm.png'),
      require('../static/basket.png'),
      require('../static/plant.png'),
    ],
    description: 'The Ceramic Bowls with Metal Tri-Stand Planter was originally designed during the post-WW2 architecture and design period. The ceramic planter is defined by its basic geometric shapesthree half-spheres and a metal triangleand its simple, clean finish. The Bowls are designed to be used in both indoor and outdoor spaces, and is supported by a powder-coated steel stand. Although this planter was originally conceived over sixty years ago, it retains a contemporary, current look. In addition to its function as a tri-planter, each of the bowls can be removed and placed on the ground, a tabletop, or other surface to create a new look.',
  },
  {
    id: shortid.generate(),
    name: 'Cube Small Planter, Clear',
    price: 8.99,
    priceBefore: 24.99,
    category: 'Home & Kitchen',
    expireAt: moment().add(45, 'm').toISOString(),
    coverPhoto: require('../static/alarm.png'),
    photos: [
      require('../static/alarm.png'),
      require('../static/basket.png'),
      require('../static/plant.png'),
    ],
    description: 'The Ceramic Bowls with Metal Tri-Stand Planter was originally designed during the post-WW2 architecture and design period. The ceramic planter is defined by its basic geometric shapesthree half-spheres and a metal triangleand its simple, clean finish. The Bowls are designed to be used in both indoor and outdoor spaces, and is supported by a powder-coated steel stand. Although this planter was originally conceived over sixty years ago, it retains a contemporary, current look. In addition to its function as a tri-planter, each of the bowls can be removed and placed on the ground, a tabletop, or other surface to create a new look.',
  },
  {
    id: shortid.generate(),
    name: 'Cube Small Planter, Clear',
    price: 8.99,
    priceBefore: 24.99,
    category: 'Home & Kitchen',
    expireAt: moment().add(45, 'm').toISOString(),
    coverPhoto: require('../static/alarm.png'),
    photos: [
      require('../static/alarm.png'),
      require('../static/basket.png'),
      require('../static/plant.png'),
    ],
    description: 'The Ceramic Bowls with Metal Tri-Stand Planter was originally designed during the post-WW2 architecture and design period. The ceramic planter is defined by its basic geometric shapesthree half-spheres and a metal triangleand its simple, clean finish. The Bowls are designed to be used in both indoor and outdoor spaces, and is supported by a powder-coated steel stand. Although this planter was originally conceived over sixty years ago, it retains a contemporary, current look. In addition to its function as a tri-planter, each of the bowls can be removed and placed on the ground, a tabletop, or other surface to create a new look.',
  },
]
