import React, { Component } from 'react'
import styled, { injectGlobal } from 'styled-components'
import { compose } from 'recompose'
import { withRouter } from 'react-router'
import Helmet from 'react-helmet'
import { GatewayProvider, GatewayDest } from 'react-gateway'
import Header from './Header'
import Footer from './FooterFinal'
import Overlay from './Overlay'

injectGlobal([`
  *, *:before, *:after {
    box-sizing: border-box;
  }

  body {
    width: 100%;
    margin: 0;
    background-color: #fafafa;
    font-family: 'Proxima Nova', ProximaNova, -apple-system, BlinkMacSystemFont, sans-serif;
    line-height: 1.4;
    color: #43484d;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: 'grayscale';
  }

  a, button, input, div {
    font-family: 'Proxima Nova', ProximaNova, -apple-system, BlinkMacSystemFont, sans-serif;
    -webkit-tap-highlight-color: transparent;
  }

  a {
    color: inherit;
    text-decoration: none;
  }
`])

const Wrap = styled.div`
  position: relative;
`

class App extends Component {
  componentWillReceiveProps (nextProps) {
    if (nextProps.location.key !== this.props.location.key && nextProps.location.state && nextProps.location.state.modal) {
      this.previousChildren = this.props.children
    }
  }

  render () {
    const { children, location, router } = this.props
    const { previousChildren } = this
    const isMobile = window && !window.matchMedia('(min-width: 40.0625em)').matches
    const isModal = location.state && location.state.modal && previousChildren && !isMobile

    return (
      <GatewayProvider>
        <Wrap>
          <Helmet
            title='Vipon'
            meta={[{ name: 'description', content: `A react app` }]}
            link={[{ rel: 'shortcut icon', href: '#' }]}
          />
          <Header />
          {isModal
            ? previousChildren
            : children
          }
          {isModal && (
            <Overlay onClose={() => router.goBack()}>
              {children}
            </Overlay>
          )}
          <GatewayDest name='dropdown' />
          <GatewayDest name='modal' />
          <Footer />
        </Wrap>
      </GatewayProvider>
    )
  }
}

export default compose(
  withRouter,
)(App)
