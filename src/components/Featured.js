import React from 'react'
import styled from 'styled-components'
import { compose, withProps } from 'recompose'
import deals from '../data/deals'
import DealList from './deal/DealList'
import Container from './common/Container'

const Wrap = styled.div`
  padding: 32px 0 64px;
`

const Featured = ({ featuredDeals }) => (
  <Container>
    <Wrap>
      <DealList deals={featuredDeals} />
    </Wrap>
  </Container>
)

export default compose(
  withProps(() => ({
    featuredDeals: deals,
  })),
)(Featured)
