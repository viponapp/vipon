import React from 'react'
import styled from 'styled-components'
import Text from './common/Text'
import Container from './common/Container'
import Input from './common/Input'
import Icon16 from './common/Icon16'
import media from '../utils/media'

const Wrap = styled.div`
`
const SellerWrap = styled.div`
  padding: 24px 0;
  color: #fff;
  text-align: center;
  background: #43484d;
`
const Content = styled.div`
  padding: 48px 0;
  ${media.medium`
    display: flex;
    align-items: flex-start;
    justify-content: space-between;
    padding: 72px 0 96px;
  `}
`
const NewsletterWrap = styled.div`
  ${media.medium`
    width: 50%;
    max-width: 400px;
  `}
`
const LinksWrap = styled.div`
  display: flex;
  padding: 48px 0 0;
  ${media.medium`
    width: 50%;
    padding: 0;
  `}
`
const Links = styled.div`
  width: 50%;
`
const Link = styled.a`
  display: block;
  margin: 12px 0 0;
  &:first-child { margin: 0; }
`
const LabelWrap = styled.div`
  padding: 0 0 16px;
`
const InputWrap = styled.div`
  position: relative;
  margin: 16px 0 0;
`
const ArrowIcon = styled.div`
  position: absolute;
  top: 7px;
  right: 0;
  fill: #43484d;
  cursor: pointer;
`

const Footer = () => (
  <Wrap>
    <SellerWrap>
      <Text micro noWrap>Sign up as a brand or seller</Text>
    </SellerWrap>
    <Container>
      <Content>
        <NewsletterWrap>
          <LabelWrap>
            <Text medium noWrap>Stay updated</Text>
          </LabelWrap>
          <Text small>Sign up for our newsletter to get the latest news, announcements, special offers and event information.</Text>
          <InputWrap>
            <Input underline placeholder='Subscribe to newsletter' />
            <ArrowIcon>
              <Icon16 arrowLeft />
            </ArrowIcon>
          </InputWrap>
        </NewsletterWrap>
        <LinksWrap>
          <Links>
            <LabelWrap>
              <Text medium noWrap>Follow us</Text>
            </LabelWrap>
            <div>
              <Link href='#'><Text micro underline noWrap>Instagram</Text></Link>
              <Link href='#'><Text micro underline noWrap>Twitter</Text></Link>
              <Link href='#'><Text micro underline noWrap>Facebook</Text></Link>
            </div>
          </Links>
          <Links>
            <LabelWrap>
              <Text medium noWrap>More</Text>
            </LabelWrap>
            <div>
              <Link href='#'><Text micro underline noWrap>Influencers</Text></Link>
              <Link href='#'><Text micro underline noWrap>Brands</Text></Link>
              <Link href='#'><Text micro underline noWrap>About</Text></Link>
            </div>
          </Links>
        </LinksWrap>
      </Content>
    </Container>
  </Wrap>
)

export default Footer
