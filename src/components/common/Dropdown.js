import React, { Component } from 'react'
import { findDOMNode } from 'react-dom'
import styled, { css } from 'styled-components'
import { Gateway } from 'react-gateway'
import { compose, withState, withHandlers } from 'recompose'
import Text from './Text'
import Container from './Container'

const Button = styled.div`
  display: inline-flex;
  align-items: center;
  height: 28px;
  padding: 0 8px;
  color: rgba(67, 72, 77, 0.6);
  fill: rgba(67, 72, 77, 0.6);
  border-radius: 2px;
  border: 1px solid #ebebeb;
  background: #fafafa;
  cursor: pointer;
  user-select: none;
  ${({ active }) => active && css`
    color: #45c5cb;
    fill: #45c5cb;
    border: 1px solid #45c5cb;
  `}
  ${({ naked }) => naked && 'border: 1px solid transparent;'}
`
const Arrow = styled.div`
  margin: 0 0 0 4px;
  border-left: 4px solid transparent;
  border-right: 4px solid transparent;
  border-top: 4px solid rgba(67, 72, 77, 0.4);
`

const Dropdown = (props) => {
  let button
  const { value, options, opened, onClick, naked } = props

  return (
    <div>
      <Button
        ref={e => { button = e }}
        onClick={onClick}
        naked={naked}
        active={!naked && value > 0}
      >
        <Text micro noWrap>{options.find(option => option.value === value).label}</Text>
        <Arrow />
      </Button>
      {opened && (
        <Gateway into='dropdown'>
          <OverlayDropdown
            target={() => button}
            {...props}
          />
        </Gateway>
      )}
    </div>
  )
}

const Overlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  background-color: rgba(0, 0, 0, 0.2);
`
const Options = styled.div`
  padding: 36px 0 0;
  max-width: 320px;
`
const OptionsPane = styled.div`
  border-radius: 2px;
  background-color: #fff;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
  overflow: hidden;
`
const Option = styled.div`
  padding: 12px;
  cursor: pointer;
  &:hover { background-color: #fafafa; }
`

class OverlayDropdown extends Component {
  state = {
    offsetTop: 0,
    offsetLeft: 0,
  }
  componentDidMount () {
    const { offsetTop, offsetLeft } = findDOMNode(this.props.target())
    this.setState({ offsetTop, offsetLeft })
  }
  render () {
    const { value, options, onClick, onOptionClick, naked } = this.props
    const { offsetTop, offsetLeft } = this.state

    return (
      <Overlay onClick={onClick}>
        <div style={{ position: 'absolute', top: offsetTop, width: '100%' }}>
          <Container>
            <Button
              naked
              active={!naked && value > 0}
              style={{ position: 'absolute', left: offsetLeft }}
            >
              <Text micro noWrap>{options.find(option => option.value === value).label}</Text>
              <Arrow />
            </Button>
            <Options>
              <OptionsPane>
                {options.map(option => (
                  <Option key={option.value} onClick={() => onOptionClick(option.value)}>
                    <Text micro noWrap>{option.label}</Text>
                  </Option>
                ))}
              </OptionsPane>
            </Options>
          </Container>
        </div>
      </Overlay>
    )
  }
}

export default compose(
  withState('opened', 'setOpened', false),
  withHandlers({
    onClick: ({ opened, setOpened }) => () => {
      setOpened(!opened)
    },
  }),
)(Dropdown)
