import styled from 'styled-components'
import media from '../../utils/media'

const Container = styled.div`
  max-width: 1360px;
  margin: 0 auto;
  padding: 0 24px;
  ${media.medium`padding: 0 48px;`}
`

export default Container
