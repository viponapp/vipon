import styled from 'styled-components'

const ButtonTransparent = styled.button`
  padding: 0;
  background: none;
  border: none;
  cursor: pointer;
  user-select: none;
  &:focus { outline: none; }
`

export default ButtonTransparent
