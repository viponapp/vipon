import React from 'react'
import styled, { keyframes } from 'styled-components'
import Text from './Text'
import { blue, lightGray } from './colors'

const Wrap = styled.button`
  overflow: hidden;
  padding: 14px 16px;
  background: none;
  border: none;
  border-radius: 2px;
  line-height: 1;
  color: inherit;
  cursor: pointer;
  user-select: none;
  letter-spacing: 0.1px;
  &:focus { outline: none; }

  ${({gray}) => gray && `
    color: ${blue};
    background-color: ${lightGray};
  `}

  ${({blue}) => blue && `
    color: #fff;
    background-color: #45c5cb;
  `}

  ${({gold}) => gold && `
    color: #fff;
    background-color: #EFC15A;
  `}

  ${({red}) => red && `
    color: #fff;
    background: #FC4F47;
  `}

  ${({facebook}) => facebook && `
    padding: 16px;
    color: #fff;
    background-color: #3b5b99;
    box-shadow: 0 0 16px #3b5b99;
  `}

  ${({twitter}) => twitter && `
    padding: 16px;
    color: #fff;
    background-color: #1da1f2;
    box-shadow: 0 0 8px #1da1f2;
  `}
`
const fadeInUp = keyframes`
  from {
    opacity: 0;
    transform: translate3d(0, 24px, 0);
  }
  to {
    opacity: 1;
    transform: translate3d(0, 0, 0);
  }
`
const AnimatedWrap = styled.div`
  ${({disabled}) => !disabled && `
    animation: ${fadeInUp} 350ms ease-out;
  `}
`

const Button = (props) => (
  <Wrap {...props}>
    <AnimatedWrap disabled={!props.fadeInUp}>
      <Text micro>{props.children}</Text>
    </AnimatedWrap>
  </Wrap>
)

export default Button
