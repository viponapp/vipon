import React from 'react'
import styled from 'styled-components'
import Icon16 from './Icon16'
import Text from './Text'

const Wrap = styled.div`
  position: relative;
  cursor: pointer;
`
const Input = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  padding: 0;
  margin: 0;
  opacity: 0;
  cursor: inherit;
  pointer-events: all;
`
const FlexContainer = styled.div`
  display: flex;
  align-items: center;
`
const Box = styled.div`
  width: 18px;
  height: 18px;
  background: #fafafa;
  border: 1px #c5c5c5 solid;
  border-radius: 2px;
  fill: #43484d;
`
const LabelWrap = styled.div`
  padding: 0 0 0 8px;
`

const Checkbox = ({ name, checked, onChange, label }) => (
  <Wrap>
    <FlexContainer>
      <Box>
        {checked && <Icon16 tick />}
      </Box>
      {label && (
        <LabelWrap>
          <Text small>{label}</Text>
        </LabelWrap>
      )}
    </FlexContainer>
    <Input type='checkbox' name={name} checked={checked} onChange={onChange} />
  </Wrap>
)

export default Checkbox
