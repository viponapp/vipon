import styled from 'styled-components'
import { blue } from './colors'

const Anchor = styled.a`
  color: ${blue};
`

export default Anchor
