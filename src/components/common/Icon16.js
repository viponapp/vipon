import React from 'react'
import styled from 'styled-components'

const Svg = styled.svg`
  display: block;
`

const Icon16 = ({ fill, search, link, arrowDown, arrowLeft, report, cart, user, tick }) => {
  let Group = <g />
  if (search) Group = <g><path fill={fill} d='M12.7,11.3c0.9-1.2,1.4-2.6,1.4-4.2C14.1,3.2,11,0,7.1,0S0,3.2,0,7.1c0,3.9,3.2,7.1,7.1,7.1 c1.6,0,3.1-0.5,4.2-1.4l3,3c0.2,0.2,0.5,0.3,0.7,0.3s0.5-0.1,0.7-0.3c0.4-0.4,0.4-1,0-1.4L12.7,11.3z M7.1,12.1 C4.3,12.1,2,9.9,2,7.1S4.3,2,7.1,2s5.1,2.3,5.1,5.1S9.9,12.1,7.1,12.1z' /></g>
  if (link) Group = <g><path fill={fill} d='M11,3h-1C9.4,3,9,3.4,9,4s0.4,1,1,1h1c1.7,0,3,1.3,3,3s-1.3,3-3,3h-1c-0.6,0-1,0.4-1,1s0.4,1,1,1h1 c2.8,0,5-2.2,5-5S13.8,3,11,3z' /><path fill={fill} d='M6,11H5c-1.7,0-3-1.3-3-3s1.3-3,3-3h1c0.6,0,1-0.4,1-1S6.6,3,6,3H5C2.2,3,0,5.2,0,8s2.2,5,5,5h1 c0.6,0,1-0.4,1-1S6.6,11,6,11z' /><path fill={fill} d='M4,8c0,0.6,0.4,1,1,1h6c0.6,0,1-0.4,1-1s-0.4-1-1-1H5C4.4,7,4,7.4,4,8z' /></g>
  if (arrowDown) Group = <g><polygon fill={fill} points='8,12.6 0.3,4.9 1.7,3.4 8,9.7 14.3,3.4 15.7,4.9 ' /></g>
  if (arrowLeft) Group = <g><polygon fill={fill} points='6.7,14.7 8.1,13.3 3.8,9 16,9 16,7 3.8,7 8.1,2.7 6.7,1.3 0,8 ' /></g>
  if (report) Group = <g><path fill={fill} d='M8,0C3.6,0,0,3.6,0,8s3.6,8,8,8s8-3.6,8-8S12.4,0,8,0z M8,12c-0.6,0-1-0.4-1-1s0.4-1,1-1s1,0.4,1,1 S8.6,12,8,12z M9,9H7V4h2V9z' /></g>
  if (cart) Group = <g><path fill={fill} d='M15,3H4.5L4,0.8C3.9,0.3,3.5,0,3,0H0v2h2.2L4,10.2C4.1,10.7,4.5,11,5,11h8c0.4,0,0.8-0.3,0.9-0.7l2-6 C16.1,3.8,15.8,3,15,3z' /><circle fill={fill} cx='5' cy='14' r='2' /><circle fill={fill} cx='13' cy='14' r='2' /></g>
  if (user) Group = <g><circle fill={fill} cx='8' cy='7' r='3' /><path fill={fill} d='M8,0C3.6,0,0,3.6,0,8s3.6,8,8,8s8-3.6,8-8S12.4,0,8,0z M12,12.4c-0.6-0.7-1.4-1.4-3-1.4H7 c-1.6,0-2.4,0.7-3,1.4C2.8,11.3,2,9.8,2,8c0-3.3,2.7-6,6-6s6,2.7,6,6C14,9.8,13.2,11.3,12,12.4z' /></g>
  if (tick) Group = <g stroke='none' strokeWidth='1' fill={fill} fillRule='evenodd'><polygon points='12.4 6 11 4.6 7 8.6 5 6.6 3.6 8 7 11.4' /></g>

  return (
    <Svg x='0px' y='0px' width='16px' height='16px' viewBox='0 0 16 16'>
      {Group}
    </Svg>
  )
}

export default Icon16
