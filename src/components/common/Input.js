import styled from 'styled-components'

const Input = styled.input`
  width: 100%;
  padding: 8px 0;
  background: none;
  border: none;
  font-size: 14px;
  line-height: 1;
  border-radius: 0;
  ${props => props.underline && 'border-bottom: 1px solid #ebebeb;'}

  &::-webkit-input-placeholder {
    color: rgba(67, 72, 77, 0.6);
  }
  &::-moz-placeholder {
    color: rgba(67, 72, 77, 0.6);
  }
  &:-ms-input-placeholder {
    color: rgba(67, 72, 77, 0.6);
  }
  &:-moz-placeholder {
    color: rgba(67, 72, 77, 0.6);
  }
  &:focus {
    outline: none;
  }
`

export default Input
