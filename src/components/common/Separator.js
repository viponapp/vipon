import styled from 'styled-components'

const Separator = styled.div`
  height: 1px;
  background: #ebebeb;
`

export default Separator
