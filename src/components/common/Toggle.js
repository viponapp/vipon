import React from 'react'
import styled, { css } from 'styled-components'
import Text from './Text'
import { compose, withState, withHandlers } from 'recompose'

const Wrap = styled.div`
  display: flex;
  align-items: center;
  height: 28px;
  padding: 0 8px;
  color: rgba(67, 72, 77, 0.6);
  fill: rgba(67, 72, 77, 0.6);
  border-radius: 2px;
  border: 1px solid #ebebeb;
  cursor: pointer;
  user-select: none;
  ${({ active }) => active && css`
    color: #45c5cb;
    fill: #45c5cb;
    border: 1px solid #45c5cb;
  `}
`

const Toggle = ({ children, onClick, active }) => (
  <Wrap onClick={onClick} active={active}>
    <Text micro noWrap>{children}</Text>
  </Wrap>
)

// temporary (demo)
export default compose(
  withState('active', 'setActive', false),
  withHandlers({
    onClick: ({ active, setActive }) => () => {
      setActive(!active)
    },
  }),
)(Toggle)
