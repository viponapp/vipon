import styled from 'styled-components'

const ButtonRound = styled.button`
  padding: 12px 16px;
  background: none;
  border: solid 1px #43484d;
  border-radius: 100px;
  font-size: 12px;
  font-weight: 600;
  line-height: 1;
  color: inherit;
  cursor: pointer;
  user-select: none;
  letter-spacing: 0.1px;
`

export default ButtonRound
