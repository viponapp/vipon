import React from 'react'
import Overlay from './Overlay'
import { Gateway } from 'react-gateway'
import styled from 'styled-components'

const Card = styled.div`

`

// NOTE: I could move everything into a huge modal, that has -margin (need close icon at multiple places)

const Modal = ({ onClose }) => (
  <Gateway into='modal'>
    <Overlay onClose={onClose}>

    </Overlay>
  </Gateway>
)

export default Modal
