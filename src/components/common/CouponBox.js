import React from 'react'
import styled from 'styled-components'
import CopyToClipboard from 'react-copy-to-clipboard'
import { compose, withState, withHandlers } from 'recompose'
import Text from './Text'
import Button from './Button'

const Box = styled.div`
  display: inline-block;
`
const Header = styled.div`
  padding: 12px 24px;
  background: rgba(109, 231, 237, 0.1);
  border: 1px solid rgba(109, 231, 237, 0.7);
  border-radius: 2px 2px 0 0;
`
const Body = styled.div`
  display: flex;
  align-items: center;
  padding: 16px 24px;
  border-left: 1px solid #eaeaea;
  border-right: 1px solid #eaeaea;
  border-bottom: 1px solid #eaeaea;
`
const Coupon = styled.div`
  color: #3fc5cb;
  text-transform: uppercase;
`
const CopyButtonWrap = styled.div`
  padding: 0 0 0 16px;
`

const CouponBox = ({ coupon, onCopy, copied }) => (
  <Box>
    <Header>
      <Text micro>Copy the code below and paste it at checkout</Text>
    </Header>
    <Body>
      <Coupon>
        <Text medium>{coupon}</Text>
      </Coupon>
      <CopyButtonWrap>
        <CopyToClipboard text={coupon} onCopy={onCopy}>
          <Button blue>{copied ? 'Copied' : 'Copy'}</Button>
        </CopyToClipboard>
      </CopyButtonWrap>
    </Body>
  </Box>
)

export default compose(
  withState('copied', 'setCopied', false),
  withHandlers({
    onCopy: ({ setCopied }) => () => {
      setCopied(true)

      // autoreset
      setTimeout(() => {
        setCopied(false)
      }, 1000)
    },
  }),
)(CouponBox)
