import styled, { css } from 'styled-components'

const Text = styled.div`
  word-wrap: break-word;
  font-weight: 400;

  ${({micro}) => micro && css`
    font-size: 12px;
    font-weight: 600;
  `}

  ${({small}) => small && css`
    font-size: 14px;
    line-height: 1.7;
  `}

  ${({medium}) => medium && css`
    font-size: 17px;
    line-height: 1.4;
  `}

  ${({large}) => large && css`
    font-size: 28px;
    line-height: 1.3;
  `}

  ${({big}) => big && css`
    font-size: 44px;
    font-weight: 600;
    letter-spacing: -0.5px;
    line-height: 1.05;
  `}

  ${({noWrap}) => noWrap && css`
    line-height: 1;
    white-space: nowrap;
  `}

  ${({red}) => red && css`
    color: #FC4F47;
  `}

  ${({gray}) => gray && css`
    color: rgba(67, 72, 77, 0.6);
  `}

  ${({lightGray}) => lightGray && css`
    color: rgba(67, 72, 77, 0.4);
  `}

  ${({underline}) => underline && css`
    text-decoration: underline;
  `}
`

export default Text
