import React from 'react'
import styled from 'styled-components'
import { compose, withState } from 'recompose'

const AspectRatioWrap = styled.div`
  position: relative;
  padding-top: 66.6666%;
`
const Image = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  background-image: url(${props => props.src});
  background-repeat: no-repeat;
  background-position: center center;
  background-size: contain;
`
const Previews = styled.div`
  display: flex;
  padding: 24px 0 0;
`
const PreviewItem = styled.div`
  width: 40px;
  height: 40px;
  margin: 0 8px 0 0;
  background-image: url(${props => props.src});
  background-repeat: no-repeat;
  background-position: center center;
  background-size: cover;
  border: 1px solid ${props => props.active ? '#87F0F4' : '#EEEEEE'};
  cursor: pointer;

  &:last-child {
    margin: 0;
  }
`

const PhotoViewer = ({ photos, index, setIndex }) => (
  <div>
    <AspectRatioWrap>
      <Image src={photos[index]} />
    </AspectRatioWrap>
    <Previews>
      {photos.map((photo, i) => (
        <PreviewItem key={i} src={photo} active={i === index} onClick={() => setIndex(i)} />
      ))}
    </Previews>
  </div>
)

export default compose(
  withState('index', 'setIndex', 0),
)(PhotoViewer)
