import React from 'react'
import styled from 'styled-components'
import { compose, withState, withHandlers } from 'recompose'
import { Link } from 'react-router'
import media from '../utils/media'
import Container from './common/Container'
import Text from './common/Text'
import ButtonRound from './common/ButtonRound'
import Icon24 from './common/Icon24'
import { gray } from './common/colors'

const Wrap = styled.div`
  background-image: linear-gradient(130deg, #fbfbfb, #f4f4f4);
`
const IconWrap = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 16px 0 0;
`
const Content = styled.div`
  padding: 8px 0 32px;
  ${media.medium`padding: 128px 0;`}
`
const Heading = styled.h1`
  margin: 0;
  padding: 0 0 12px;
`
const TaglineWrap = styled.div`
  padding: 0 0 24px;
`
const CloseIcon = styled.div`
  cursor: pointer;
`

const Hero = ({ toggleHidden, hidden }) => (
  <div>
    {!hidden && (
      <Wrap>
        <Container>
          <IconWrap>
            <CloseIcon onClick={toggleHidden}>
              <Icon24 close fill={gray} />
            </CloseIcon>
          </IconWrap>
          <Content>
            <Heading>
              <Text big>Shop Smarter on Amazon</Text>
            </Heading>
            <TaglineWrap>
              <Text medium gray>Vipon provides you with the best brands and deals for your influence</Text>
            </TaglineWrap>
            <Link to='/join'>
              <ButtonRound>Create your account</ButtonRound>
            </Link>
          </Content>
        </Container>
      </Wrap>
    )}
  </div>
)

export default compose(
  withState('hidden', 'setHidden', false),
  withHandlers({
    toggleHidden: ({ hidden, setHidden }) => () => {
      setHidden(!hidden)
    },
  }),
)(Hero)
