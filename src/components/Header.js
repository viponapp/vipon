import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router'
import { compose, withState, withHandlers } from 'recompose'
import media from '../utils/media'
import Container from './common/Container'
import Button from './common/Button'
import Logo from './common/Logo'
import Input from './common/Input'
import Icon16 from './common/Icon16'
import Icon24 from './common/Icon24'
import { darkBlue } from './common/colors'

const Wrap = styled.div`
  padding: 16px 0;
  background: #fff;
  border-bottom: 1px solid #ebebeb;
`
const Content = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`
const RightWrap = styled.div`
  display: flex;
  align-items: center;
`
const SearchIcon = styled.div`
  margin: 0 8px 0;
  ${media.medium`pointer-events: none;`}
`
const MediumInput = styled.div`
  display: none;
  width: 240px;
  ${media.medium`display: block;`}
`
const FlexWrap = styled.div`
  display: flex;
  align-items: center;
`
const RequestsWrap = styled.div`
  padding: 0 16px 0 0;
`
const SearchWrap = styled.div`
  width: 100%;
  padding: 0 0 0 8px;
`

const Header = ({ searchVisible, toggleSearch, userVisible }) => (
  <Wrap>
    <Container>
      <Content>
        <RightWrap>
          <Link to='/'>
            <Logo />
          </Link>
          <SearchIcon onClick={toggleSearch}>
            {!searchVisible && <Icon16 search fill='rgba(67, 72, 77, 0.6)' />}
            {searchVisible && <Icon24 closeMini fill='rgba(67, 72, 77, 0.6)' />}
          </SearchIcon>
          <MediumInput>
            <Input placeholder='Search for deals...' />
          </MediumInput>
        </RightWrap>
        {!searchVisible && !userVisible ? (
          <FlexWrap>
            <RequestsWrap>
              <Link to='/requests'>
                <Icon16 cart fill={darkBlue} />
              </Link>
            </RequestsWrap>
            <Link to='/account'>
              <Icon16 user fill={darkBlue} />
            </Link>
          </FlexWrap>
        ) : (
          <FlexWrap>
            <Link to='/login'>
              <Button>Log in</Button>
            </Link>
            <Link to='/join'>
              <Button blue>Join</Button>
            </Link>
          </FlexWrap>
        )}
        {searchVisible && (
          <SearchWrap>
            <Input placeholder='Search for deals...' />
          </SearchWrap>
        )}
      </Content>
    </Container>
  </Wrap>
)

export default compose(
  withState('searchVisible', 'setHidden', false),
  withHandlers({
    toggleSearch: ({ searchVisible, setHidden }) => () => {
      setHidden(!searchVisible)
    },
  }),
)(Header)
