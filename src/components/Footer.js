import React from 'react'
import styled from 'styled-components'
import Text from './common/Text'
import Container from './common/Container'
import Logo from './common/Logo'
import Button from './common/Button'
import Input from './common/Input'
import media from '../utils/media'

const SellerWrap = styled.div`
  display: flex;
  justify-content: center;
  padding: 24px 0;
  background: #ebebeb;
  cursor: pointer;
`
const Content = styled.div`
  padding: 48px 0 32px;
  background: #f5f5f5;
`
const FlexContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`
const Item = styled.div`
  width: 100%;
  ${media.medium`
    width: 25%;
    max-width: 360px;
  `}
  padding: 0 0 48px;
  &:last-child { padding: 0;}
`
const LabelWrap = styled.div`
  padding: 0 0 12px;
`
const InputWrap = styled.div`
  padding: 16px 0 0;
`
const LinkWrap = styled.div`
  padding: 0 0 8px;
  &:last-child { padding: 0; }
`
const Link = styled.a`
  display: inline-block;
`
const ButtonWrap = styled.div`
  padding: 16px 0 0;
`

const Footer = () => (
  <div>
    <SellerWrap>
      <Text micro noWrap>Sign up as a brand or seller</Text>
    </SellerWrap>
    <Content>
      <Container>
        <FlexContainer>
          <Item>
            <LabelWrap>
              <Text micro lightGray noWrap>Stay updated</Text>
            </LabelWrap>
            <Text medium>Sign up for our newsletter to get the latest news, announcements, special offers and event information.</Text>
            <InputWrap>
              <Input placeholder='Subscribe to newsletter' />
            </InputWrap>
          </Item>
          <Item>
            <FlexContainer>
              <div>
                <LabelWrap>
                  <Text micro lightGray noWrap>Follow</Text>
                </LabelWrap>
                <LinkWrap>
                  <Link href='#'>
                    <Text medium noWrap>Instagram</Text>
                  </Link>
                </LinkWrap>
                <LinkWrap>
                  <Link href='#'>
                    <Text medium noWrap>Twitter</Text>
                  </Link>
                </LinkWrap>
                <LinkWrap>
                  <Link href='#'>
                    <Text medium noWrap>Facebook</Text>
                  </Link>
                </LinkWrap>
              </div>
              <div>
                <LabelWrap>
                  <Text micro lightGray noWrap>More</Text>
                </LabelWrap>
                <LinkWrap>
                  <Link href='#'>
                    <Text medium noWrap>Influencers</Text>
                  </Link>
                </LinkWrap>
                <LinkWrap>
                  <Link href='#'>
                    <Text medium noWrap>Brands</Text>
                  </Link>
                </LinkWrap>
                <LinkWrap>
                  <Link href='#'>
                    <Text medium noWrap>About</Text>
                  </Link>
                </LinkWrap>
                <LinkWrap>
                  <Link href='#'>
                    <Text medium noWrap>Support</Text>
                  </Link>
                </LinkWrap>
              </div>
            </FlexContainer>
          </Item>
          <Item>
            <LabelWrap>
              <Logo blue />
            </LabelWrap>
            <Text medium>Use your social influence to get free products and earn from your impact.</Text>
            <ButtonWrap>
              <Button blue>Become an influencer</Button>
            </ButtonWrap>
          </Item>
        </FlexContainer>
      </Container>
    </Content>
  </div>
)

export default Footer
