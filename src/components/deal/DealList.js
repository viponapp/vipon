import React from 'react'
import styled from 'styled-components'
import DealItem from './DealItem'
import media from '../../utils/media'

const List = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: -8px;
`
const ItemWrap = styled.div`
  width: 100%;
  ${media.medium`width: 50%;`}
  ${media.desktop`width: 33.3333%;`}
  padding: 8px;
`

const DealList = ({ deals }) => (
  <List>
    {deals.map((deal, i) => (
      <ItemWrap key={i}>
        <DealItem deal={deal} />
      </ItemWrap>
    ))}
  </List>
)

export default DealList
