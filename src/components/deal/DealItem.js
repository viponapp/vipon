import React from 'react'
import styled from 'styled-components'
import moment from 'moment'
import slug from 'slugg'
import { Link as RouterLink } from 'react-router'
import Text from '../common/Text'

const Link = styled(RouterLink)`
  display: block;
  padding: 0 24px;
  background: #fff;
`
const ExpiryWrap = styled.div`
  padding: 24px 0 8px;
`
const AspectRatioWrap = styled.div`
  position: relative;
  padding-top: 66.6666%;
`
const Image = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  background-image: url(${props => props.src});
  background-repeat: no-repeat;
  background-position: center center;
  background-size: contain;
`
const InfoWrap = styled.div`
  padding: 24px 0;
`
const CategoryWrap = styled.div`
  padding: 0 0 2px;
`
const NameWrap = styled.div`
  padding: 0 0 6px;
`
const PricesWrap = styled.div`
  display: flex;
`
const BeforeWrap = styled.div`
  padding: 0 0 0 8px;
`

const DealItem = ({ deal }) => (
  <Link to={{ pathname: `/${slug(deal.name)}-${deal.id}`, state: { modal: true } }}>
    <ExpiryWrap>
      <Text micro red noWrap>{`Expires ${moment(deal.expireAt).fromNow()}`}</Text>
    </ExpiryWrap>
    <AspectRatioWrap>
      <Image src={deal.coverPhoto} />
    </AspectRatioWrap>
    <InfoWrap>
      <CategoryWrap>
        <Text micro lightGray noWrap>{deal.category}</Text>
      </CategoryWrap>
      <NameWrap>
        <Text medium>{deal.name}</Text>
      </NameWrap>
      <PricesWrap>
        <Text medium noWrap>{`$${deal.price}`}</Text>
        <BeforeWrap>
          <Text micro lightGray noWrap><s>{`$${deal.priceBefore}`}</s></Text>
        </BeforeWrap>
      </PricesWrap>
    </InfoWrap>
  </Link>
)

export default DealItem
