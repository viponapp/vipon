import React from 'react'
import styled from 'styled-components'
import moment from 'moment'
import Dotdotdot from 'react-dotdotdot'
import { compose, withState, withHandlers } from 'recompose'
import CopyToClipboard from 'react-copy-to-clipboard'
import media from '../../utils/media'
import Text from '../common/Text'
import Button from '../common/Button'
import Separator from '../common/Separator'
import PhotoViewer from '../PhotoViewer'
import Icon16 from '../common/Icon16'
import Icon24 from '../common/Icon24'
import Anchor from '../common/Anchor'
import ButtonTransparent from '../common/ButtonTransparent'
import { gray } from '../common/colors'

const Wrap = styled.div`
  background: #fff;
`
const ExpiryWrap = styled.div`
  display: flex;
  justify-content: center;
  padding: 16px 0;
  background-color: #fafafa;
`
const Content = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  align-items: flex-start;
  padding: 24px;
  ${media.medium`padding: 128px 24px;`}
`
const Section = styled.div`
  width: 100%;
  padding: 0 0 24px;
  ${media.medium`
    width: 40%;
    padding: 0;
  `}

  &:last-child {
    padding: 0;
  }
`
const InfoWrap = styled.div`
  width: 100%;
  ${media.medium`
    width: 40%;
    max-width: 480px;
  `}
`
const CategoryWrap = styled.div`
  padding: 0 0 4px;
`
const PricesWrap = styled.div`
  display: flex;
  padding: 14px 0 0;
`
const PriceWrap = styled.div`
  padding: 0 4px 0 0;
`
const ActionSection = styled.div`
  display: flex;
  align-items: center;
  padding: 16px 0 0;
`

const ActionSectionSpacing = styled.div`
  padding: 0 0 0 16px;
  line-height: 1;
`
const SeparatorWrap = styled.div`
  padding: 32px 0;
`
const Description = styled.div`
  padding: 8px 0 8px;
`
const MoreWrap = styled.div`
  cursor: pointer;
`
const CopyWrap = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;

  svg {
    margin: 0 4px 0 0;
  }
`
const Share = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`
const Social = styled.div`
  display: flex;
  align-items: center;
`
const SocialItem = styled.div`
  margin: 0 16px 0 0;
  cursor: pointer;
  fill: #e5e5e5;

  &:last-child {
    margin: 0;
  }
`

const DealDetail = ({ deal, toggleExpanded, expanded, onCopy, copied }) => (
  <Wrap>
    <ExpiryWrap>
      <Text micro noWrap>{`Coupon code expires ${moment(deal.expireAt).fromNow()}`}</Text>
    </ExpiryWrap>
    <Content>
      <Section>
        <PhotoViewer photos={deal.photos} />
      </Section>
      <InfoWrap>
        <CategoryWrap>
          <Text micro lightGray noWrap>{deal.category}</Text>
        </CategoryWrap>
        <Text large>{deal.name}</Text>
        <PricesWrap>
          <PriceWrap>
            <Text large noWrap>$44</Text>
          </PriceWrap>
          <PriceWrap>
            <Text micro noWrap><s>$88</s></Text>
          </PriceWrap>
          <Text micro red noWrap> 50% discount</Text>
        </PricesWrap>
        <ActionSection>
          <Button blue>GET THE DEAL — $44</Button>
          <ActionSectionSpacing>
            <Text micro noWrap><Anchor href='#'>Open in Amazon</Anchor></Text>
          </ActionSectionSpacing>
          <ActionSectionSpacing>
            <ButtonTransparent title='Report deal'>
              <Icon16 report fill={gray} />
            </ButtonTransparent>
          </ActionSectionSpacing>
        </ActionSection>
        <SeparatorWrap>
          <Separator />
        </SeparatorWrap>
        <Text micro noWrap>Description</Text>
        <Description>
          {expanded && (
            <Text small>{deal.description}</Text>
          )}
          {!expanded && (
            <Dotdotdot clamp={4}>
              <Text small>{deal.description}</Text>
            </Dotdotdot>
          )}
        </Description>
        <MoreWrap onClick={toggleExpanded}>
          <Text micro gray noWrap>Show {expanded ? 'less' : 'more'}</Text>
        </MoreWrap>
        <SeparatorWrap>
          <Separator />
        </SeparatorWrap>
        <Share>
          <CopyToClipboard text={`https://vipon.com`} onCopy={onCopy}>
            <CopyWrap>
              <Icon16 link fill='#43484d' />
              <Text micro gray noWrap>
                <span>Copy link</span>
                {copied && <span> (Copied)</span>}
              </Text>
            </CopyWrap>
          </CopyToClipboard>
          <Social>
            {[<Icon24 facebook />, <Icon24 pinterest />, <Icon24 twitter />, <Icon24 mail />].map((icon, i) => (
              <SocialItem key={i}>
                {icon}
              </SocialItem>
            ))}
          </Social>
        </Share>
      </InfoWrap>
    </Content>
  </Wrap>
)

export default compose(
  withState('expanded', 'setExpanded', false),
  withState('copied', 'setCopied', false),
  withHandlers({
    toggleExpanded: ({ setExpanded, expanded }) => () => (
      setExpanded(!expanded)
    ),
    onCopy: ({ setCopied }) => () => {
      setCopied(true)

      // autoreset
      setTimeout(() => {
        setCopied(false)
      }, 1000)
    },
  }),
)(DealDetail)
