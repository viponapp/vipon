import React from 'react'
import styled from 'styled-components'
import media from '../../utils/media'
import Text from '../common/Text'
import Separator from '../common/Separator'
import Button from '../common/Button'
import CouponBox from '../common/CouponBox'

const Item = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 24px;
  background: #fff;
  border-bottom: 1px #F6F6F7 solid;
`
const Img = styled.div`
  width: 64px;
  height: 64px;
  background-size: cover;
  background-position: center center;
  background-repeat: no-repeat;
  background-image: url(${props => props.src});
`
const LeftContainer = styled.div`
  display: flex;
  width: 100%;
  ${media.medium`width: 56%;`}
`
const InfoWrap = styled.div`
  padding: 0 0 0 24px;
  ${media.medium`padding: 0 24px;`}
`
const PriceContainer = styled.div`
  display: flex;
  padding: 8px 0 0;
`
const PriceWrap = styled.div`
  padding: 0 0 0 8px;
`
const SeparatorWrap = styled.div`
  padding: 16px 0;
`
const InstructionsBodyWrap = styled.div`
  padding: 8px 0 0;
`
const SellerEmailWrap = styled.div`
  padding: 8px 0 0;
  color: #3fc5cb;
`
const ReportWrap = styled.div`
  padding: 16px 0 0;
  color: #c5c5c5;
  text-decoration: underline;
`
const RightContainer = styled.div`
  display: flex;
  flex-flow: wrap;
  justify-content: flex-end;
  width: 100%;
  padding: 24px 0 0;
  ${media.medium`
    width: 44%;
    padding: 0;
  `}
`
const RequestedContainer = styled.div`
  width: 50%;
`
const StatusContainer = styled.div`
  width: 50%;
`
const StatusWrap = styled.div`
  display: flex;
  align-items: center;
`
const StatusPointWrap = styled.div`
  padding: 0 0 0 8px;
`
const StatusPoint = styled.div`
  width: 8px;
  height: 8px;
  border-radius: 50%;
  ${props => props.green && `background: #19a974;`}
  ${props => props.red && `background: #fc4f47;`}
  ${props => props.yellow && `background: #fbf1a9;`}
`
const RemoveRequestWrap = styled.div`
  padding: 24px 0 0;
`
const ApproveDeclineWrap = styled.div`
  width: 100%;
  padding: 24px 0 0;
`
const ApprovedWrap = styled.div`
  width: 100%;
  padding: 24px 0 0;
`
const AmazonButtonWrap = styled.div`
  padding: 8px 0 0;
`

const RequestItem = ({ deal, requested, status, accepted, coupon }) => (
  <Item>
    <LeftContainer>
      <div>
        <Img src={deal.coverPhoto} />
      </div>
      <InfoWrap>
        <Text medium>{deal.name}</Text>
        <PriceContainer>
          <Text medium noWrap>{`$${deal.finalPrice}`}</Text>
          <PriceWrap>
            <Text micro lightGray noWrap><s>{`$${deal.price}`}</s></Text>
          </PriceWrap>
        </PriceContainer>
        {status === 'approved' && (
          <div>
            <SeparatorWrap>
              <Separator />
            </SeparatorWrap>
            <Text micro noWrap>Seller Instructions</Text>
            <InstructionsBodyWrap>
              <Text small>You are not required to leave a review for this product. However, you are able to leave reviews for any products you purchase on Amazon if you choose to.</Text>
            </InstructionsBodyWrap>
            <SeparatorWrap>
              <Separator />
            </SeparatorWrap>
            <Text micro noWrap>Seller Contact</Text>
            <SellerEmailWrap>
              <Text micro noWrap>
                <a target='_blank' href={`mailto:${deal.supportEmail}`}>{deal.supportEmail}</a>
              </Text>
            </SellerEmailWrap>
            <ReportWrap>
              <Text micro noWrap>Report this deal</Text>
            </ReportWrap>
          </div>
        )}
      </InfoWrap>
    </LeftContainer>
    <RightContainer>
      <RequestedContainer>
        <Text small noWrap>{requested}</Text>
      </RequestedContainer>
      <StatusContainer>
        <StatusWrap>
          <Text small noWrap>
            {status === 'approved' && 'Approved'}
            {status === 'pending' && 'Pending'}
            {status === 'denied' && 'Not Approved'}
          </Text>
          <StatusPointWrap>
            <StatusPoint
              green={status === 'approved'}
              yellow={status === 'pending'}
              red={status === 'denied'}
            />
          </StatusPointWrap>
        </StatusWrap>
      </StatusContainer>
      {status === 'pending' && (
        <RemoveRequestWrap>
          <Button red>REMOVE REQUEST</Button>
        </RemoveRequestWrap>
      )}
      {status === 'approved' && !accepted && (
        <ApproveDeclineWrap>
          <Button blue>Accept voucher</Button>
          <Button>Decline voucher</Button>
        </ApproveDeclineWrap>
      )}
      {accepted && (
        <ApprovedWrap>
          <div>
            <CouponBox coupon={coupon} />
          </div>
          <AmazonButtonWrap>
            <Button gold>GO TO AMAZON</Button>
          </AmazonButtonWrap>
        </ApprovedWrap>
      )}
    </RightContainer>
  </Item>
)

export default RequestItem
