import React from 'react'
import styled from 'styled-components'
import RequestItem from './RequestItem'
import Text from '../common/Text'
import media from '../../utils/media'

const List = styled.div`
`
const Header = styled.div`
  display: none;
  ${media.medium`display: flex;`}
  padding: 0 24px 12px;
  color: #c5c5c5;
`
const ProductLabelWrap = styled.div`
  width: 56%;
`
const RequestedLabelWrap = styled.div`
  width: 22%;
`
const StatusLabelWrap = styled.div`
  width: 22%;
`

const RequestList = ({ requests }) => (
  <List>
    <Header>
      <ProductLabelWrap>
        <Text micro noWrap>PRODUCT</Text>
      </ProductLabelWrap>
      <RequestedLabelWrap>
        <Text micro noWrap>REQUESTED</Text>
      </RequestedLabelWrap>
      <StatusLabelWrap>
        <Text micro noWrap>STATUS</Text>
      </StatusLabelWrap>
    </Header>
    {requests.map((request, i) => (
      <RequestItem key={i} {...request} />
    ))}
  </List>
)

export default RequestList
