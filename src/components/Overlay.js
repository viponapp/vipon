import React from 'react'
import styled from 'styled-components'
import KeyHandler from 'react-key-handler'
import { compose, lifecycle } from 'recompose'

// NOTE: SO THERE IS THIS ISSUE WITH COMPONENT-FIRST DEVELOPMENT THAT
// THERE NEEDS TO GO A LOT OF THOUGHT INTO COMPONENTS – SOMETHING WE
// DON'T HAVE TIME FOR WHEN BUILDING AN APP QUICKLY. WE CAN STILL SEE
// LATER WHERE WE CAN BREAK THINGS INTO COMPONENTS TO REUSE THEM FAST IN THE
// FUTURE. DON'T CODE COMPONENTS FOR THE FUTURE. NO OVER-COMPONENTISATION.
// IF I GOT THE SAME SCHEME POPPING UP OVER AND OVER AGAIN, THEN I CAN PUT IT
// IN A COMPONENT. AND ONLY IF IT MAKES THINGS FASTER > CONSISTENT.

// NOTE: HOW CAN I AVOID REDOING THE MODAL AND END UP ALMOST AT THE SAME
// SOLUTION AGAIN?

const Backdrop = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  overflow-x: hidden;
  overflow-y: auto;
  background-color: rgba(0, 0, 0, 0.6);
`
const Content = styled.div`
  position: relative;
`

const Overlay = ({ children, onClose }) => (
  <Backdrop onClick={onClose}>
    <KeyHandler
      keyEventName='keyup'
      keyValue='Escape'
      onKeyHandle={onClose}
    />
    <Content>
      {React.cloneElement(children, { overlay: true })}
    </Content>
  </Backdrop>
)

// block body from being scrolled
export default compose(
  lifecycle({
    componentDidMount () {
      document.body.style.top = `-${window.pageYOffset}px`
      document.body.style.position = 'fixed'
    },
    componentWillUnmount () {
      document.body.style.top = ''
      document.body.style.position = ''
    },
  }),
)(Overlay)
