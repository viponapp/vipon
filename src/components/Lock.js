import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router'
import Text from './common/Text'
import Container from './common/Container'
import Button from './common/Button'
import Input from './common/Input'
import { compose, withState } from 'recompose'

const Wrap = styled.div`
  padding: 48px 0;
  background-color: #fff;
`
const Content = styled.div`
  width: 100%;
  max-width: 360px;
`
const HeadingWrap = styled.div`
  padding: 0 0 8px;
`
const FacebookWrap = styled.div`
  padding: 16px 0 0;
`
const TwitterWrap = styled.div`
  padding: 12px 0 0;
`
const Form = styled.div`
  padding: 32px 0 0;
`
const EmailWrap = styled.div`
  padding: 0 0 16px;
`
const PasswordWrap = styled.div`
  padding: 0 0 12px;
`
const NoteWrap = styled.div`
  padding: 16px 0 0;
`

const Lock = ({ signup, login, emailOpenend, setEmailOpened }) => (
  <Wrap>
    <Container>
      <Content>
        <HeadingWrap>
          {signup && <Text large>Sign up</Text>}
          {login && <Text large>Log in</Text>}
        </HeadingWrap>
        {signup && <Text medium gray>Join Vipon to get great deals on products and more.</Text>}
        <FacebookWrap>
          {signup && <Button facebook>Sign up with Facebook</Button>}
          {login && <Button facebook>Log in with Facebook</Button>}
        </FacebookWrap>
        <TwitterWrap>
          {signup && <Button twitter>Sign up with Twitter</Button>}
          {login && <Button twitter>Log in with Twitter</Button>}
        </TwitterWrap>
        <TwitterWrap>
          {!emailOpenend && signup && <Button onClick={() => setEmailOpened(!emailOpenend)}>Sign up with Email</Button>}
          {!emailOpenend && login && <Button onClick={() => setEmailOpened(!emailOpenend)}>Log in with Email</Button>}
        </TwitterWrap>
        {emailOpenend && (
          <Form>
            <EmailWrap>
              <Input underline type='email' placeholder='Email' />
            </EmailWrap>
            <PasswordWrap>
              <Input underline type='password' placeholder='Password' />
            </PasswordWrap>
            {signup && <Button blue>Sign up with Email</Button>}
            {login && <Button blue>Log in with Email</Button>}
          </Form>
        )}
        <NoteWrap>
          {signup && (
            <Link to='/login'>
              <Text micro lightGray noWrap>Already have an account?</Text>
            </Link>
          )}
          {login && (
            <Link to='join'>
              <Text micro lightGray noWrap>Don't have an account yet?</Text>
            </Link>
          )}
        </NoteWrap>
      </Content>
    </Container>
  </Wrap>
)

export default compose(
  withState('emailOpenend', 'setEmailOpened', false),
)(Lock)
