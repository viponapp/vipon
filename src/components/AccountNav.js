import React from 'react'
import styled from 'styled-components'
import { Link as RouterLink } from 'react-router'
import Container from './common/Container'
import Text from './common/Text'

const Nav = styled.div`
  padding: 20px 0;
  background-image: linear-gradient(-80deg, #52ABD4 0%, #3FC5CB 100%);
`
const FlexContainer = styled.div`
  display: flex;
`
const FlexItem = styled.div`
  padding: 0 32px 0 0;
`
const Link = styled(RouterLink)`
  color: rgba(255, 255, 255, 0.7);
  &.active { color: #fff; }
`

const AccountNav = () => (
  <Nav>
    <Container>
      <FlexContainer>
        <FlexItem>
          <Link to='/requests' activeClassName='active'>
            <Text micro noWrap>Deal Requests</Text>
          </Link>
        </FlexItem>
        <FlexItem>
          <Link to='/account' activeClassName='active'>
            <Text micro noWrap>Account Details</Text>
          </Link>
        </FlexItem>
      </FlexContainer>
    </Container>
  </Nav>
)

export default AccountNav
