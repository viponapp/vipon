import React from 'react'
import styled, { css } from 'styled-components'
import { compose, withState } from 'recompose'
import Dropdown from './common/Dropdown'
import Toggle from './common/Toggle'
import Icon24 from './common/Icon24'
import media from '../utils/media'

const Wrap = styled.div`
  padding: 16px 0 0;
`
const ScrollWrap = styled.div`
  ${({ disabled }) => !disabled && 'height: 28px;'}
  overflow: hidden;
`
const ScrollContainer = styled.div`
  ${({ disabled }) => !disabled && css`
    -webkit-overflow-scrolling: touch;
    padding: 0 0 20px;
    overflow-x: auto;
  `}
`
const FiltersWrap = styled.div`
  display: flex;
  ${({ wrap }) => wrap && 'flex-wrap: wrap;'}
`
const FilterWrap = styled.div`
  margin: 0 8px 8px 0;
`
const ExpandWrap = styled.div`
  ${media.medium`display: none;`}
  display: flex;
  justify-content: flex-end;
`
const ExpandIcon = styled.div`
  fill: rgba(67, 72, 77, 0.6);
`

const Filters = ({ sortValue, setSortValue, priceValue, setPriceValue, expanded, setExpanded }) => (
  <Wrap>
    <ScrollWrap disabled={expanded}>
      <ScrollContainer disabled={expanded}>
        <FiltersWrap wrap={expanded}>
          {[
            <Dropdown
              naked
              value={sortValue}
              options={[{ value: 0, label: 'Expiry' }, { value: 1, label: 'Price (Low)' }, { value: 2, label: 'Price (High)' }]}
              onOptionClick={value => setSortValue(value)}
            />,
            <Dropdown
              value={priceValue}
              options={[{ value: 0, label: 'Any price' }, { value: 1, label: '$0 - $50' }, { value: 2, label: '$50 - $100' }, { value: 3, label: '$100 - $200' }, { value: 4, label: '$200+' }]}
              onOptionClick={value => setPriceValue(value)}
            />,
            <Toggle>Fashion</Toggle>,
            <Toggle>Beauty</Toggle>,
            <Toggle>Tech</Toggle>,
            <Toggle>Kitchen</Toggle>,
            <Toggle>Toys</Toggle>,
            <Toggle>Office</Toggle>,
            <Toggle>Home</Toggle>,
            <Toggle>Hiking / camping</Toggle>,
            <Toggle>Garden</Toggle>,
            <Toggle>Food</Toggle>,
          ].map((item, i) => (
            <FilterWrap key={i}>
              {item}
            </FilterWrap>
          ))}
        </FiltersWrap>
      </ScrollContainer>
    </ScrollWrap>
    <ExpandWrap>
      <ExpandIcon onClick={() => setExpanded(!expanded)}>
        <Icon24 more />
      </ExpandIcon>
    </ExpandWrap>
  </Wrap>
)

// NOTE: Move into global state
export default compose(
  withState('sortValue', 'setSortValue', 0),
  withState('priceValue', 'setPriceValue', 0),
  withState('expanded', 'setExpanded', false),
)(Filters)
